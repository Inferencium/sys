# Copyright 2020 Gentoo Authors, 2023 Jake Winters
# SPDX-License-Identifier: GPL-2.0-only


EAPI="8"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2 verify-sig
detect_version

DESCRIPTION="Linux kernel source code"
HOMEPAGE="https://www.kernel.org/"
SRC_URI="
	https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/${P}.tar.xz
	verify-sig? (
		https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/${P}.tar.sign
	)
"

LICENSE="GPL-2"
KEYWORDS="amd64 arm64"

BDEPEND="
	verify-sig? ( sec-keys/openpgp-keys-kernel )
"

VERIFY_SIG_OPENPGP_KEY_PATH=${BROOT}/usr/share/openpgp-keys/kernel.org.asc

src_unpack() {
	if use verify-sig; then
		einfo "Unpacking ${P}.tar.xz ..."
		verify-sig_verify_detached - "${DISTDIR}"/${P}.tar.sign \
			< <(xz -cd "${DISTDIR}"/${P}.tar.xz | tee >(tar -x))
		assert "Unpack failed"
	else
		default
	fi
}
